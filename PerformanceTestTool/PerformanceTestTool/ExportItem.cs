﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTestTool
{
    public class ExportItem
    {
        public string ContainerName { get; set; }

        public string ContentBlobId { get; set; }

        public string ContentFileName { get; set; }

        public string ContentFormat { get; set; }
        
        public long ContentSize { get; set; }

        public Dictionary<string, object> ToDictionary()
        {
            var dic = new Dictionary<string, object>();
            dic.Add("ContainerName", ContainerName);
            dic.Add("ContentBlobId", ContentBlobId);
            dic.Add("ContentFileName", ContentFileName);
            dic.Add("ContentFormat", ContentFormat);
            dic.Add("ContentSize", ContentSize);

            return dic;
        }
    }

    public class CsvExport
    {
        private List<string> fields = new List<string> { "ContainerName", "ContentBlobId", "ContentFileName", "ContentFormat", "ContentSize" };

        private List<ExportItem> exportItems = new List<ExportItem>();

        public void Add(ExportItem item)
        {
            exportItems.Add(item);
        }

        public void Reset()
        {
            exportItems.Clear();
        }

        public async Task Export()
        {
            StringBuilder sb = new StringBuilder();
            //foreach (string field in fields)
            //{
            //    sb.Append(field).Append(",");
            //}
            //sb.AppendLine();

            foreach (var exportItem in exportItems)
            {
                var dic = exportItem.ToDictionary();
                foreach (var field in fields)
                {
                    sb.Append(dic[field]).Append(",");
                }
                sb.Remove(sb.Length - 1, 1).Append("\n");
            }
            File.AppendAllText("UploadFiles.csv", sb.ToString());
        }
    }
}
