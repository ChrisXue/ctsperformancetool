﻿namespace PerformanceTestTool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lbl_fileList = new System.Windows.Forms.Label();
            this.btn_Browse = new System.Windows.Forms.Button();
            this.btn_Upload = new System.Windows.Forms.Button();
            this.lb_uploadfiles = new System.Windows.Forms.ListBox();
            this.ofd_UploadFileSelector = new System.Windows.Forms.OpenFileDialog();
            this.txt_State = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_Condition = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(598, 473);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage2.Controls.Add(this.txt_Condition);
            this.tabPage2.Controls.Add(this.txt_State);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.lbl_fileList);
            this.tabPage2.Controls.Add(this.btn_Browse);
            this.tabPage2.Controls.Add(this.btn_Upload);
            this.tabPage2.Controls.Add(this.lb_uploadfiles);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(590, 447);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "UploadFile";
            // 
            // lbl_fileList
            // 
            this.lbl_fileList.AutoSize = true;
            this.lbl_fileList.Location = new System.Drawing.Point(24, 13);
            this.lbl_fileList.Name = "lbl_fileList";
            this.lbl_fileList.Size = new System.Drawing.Size(35, 13);
            this.lbl_fileList.TabIndex = 0;
            this.lbl_fileList.Text = "Filelist";
            // 
            // btn_Browse
            // 
            this.btn_Browse.Location = new System.Drawing.Point(457, 5);
            this.btn_Browse.Name = "btn_Browse";
            this.btn_Browse.Size = new System.Drawing.Size(61, 28);
            this.btn_Browse.TabIndex = 2;
            this.btn_Browse.Text = "Browse";
            this.btn_Browse.UseVisualStyleBackColor = true;
            this.btn_Browse.Click += new System.EventHandler(this.btn_Browse_Click);
            // 
            // btn_Upload
            // 
            this.btn_Upload.Location = new System.Drawing.Point(457, 233);
            this.btn_Upload.Name = "btn_Upload";
            this.btn_Upload.Size = new System.Drawing.Size(61, 28);
            this.btn_Upload.TabIndex = 2;
            this.btn_Upload.Text = "Upload";
            this.btn_Upload.UseVisualStyleBackColor = true;
            this.btn_Upload.Click += new System.EventHandler(this.btn_Upload_Click);
            // 
            // lb_uploadfiles
            // 
            this.lb_uploadfiles.FormattingEnabled = true;
            this.lb_uploadfiles.Location = new System.Drawing.Point(27, 39);
            this.lb_uploadfiles.Name = "lb_uploadfiles";
            this.lb_uploadfiles.Size = new System.Drawing.Size(491, 173);
            this.lb_uploadfiles.TabIndex = 3;
            // 
            // ofd_UploadFileSelector
            // 
            this.ofd_UploadFileSelector.FileName = "openFileDialog1";
            // 
            // txt_State
            // 
            this.txt_State.Location = new System.Drawing.Point(62, 233);
            this.txt_State.Multiline = true;
            this.txt_State.Name = "txt_State";
            this.txt_State.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_State.Size = new System.Drawing.Size(260, 126);
            this.txt_State.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 236);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "State";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 382);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Condition";
            // 
            // txt_Condition
            // 
            this.txt_Condition.Location = new System.Drawing.Point(62, 378);
            this.txt_Condition.Name = "txt_Condition";
            this.txt_Condition.Size = new System.Drawing.Size(371, 20);
            this.txt_Condition.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 473);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.OpenFileDialog ofd_UploadFileSelector;
        private System.Windows.Forms.ListBox lb_uploadfiles;
        private System.Windows.Forms.Button btn_Upload;
        private System.Windows.Forms.Button btn_Browse;
        private System.Windows.Forms.Label lbl_fileList;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_State;
        private System.Windows.Forms.TextBox txt_Condition;
        private System.Windows.Forms.Label label2;
    }
}

