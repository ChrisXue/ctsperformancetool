﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nuage.Common.Model.Util.Common;

namespace PerformanceTestTool
{
    public class ConfigTool :IConfig
    {
        public string GetConfigItem(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        public string GetConfigurationSetting(string configurationSetting, string defaultValue, bool throwIfNull)
        {
            throw new NotImplementedException();
        }

        public void RemoveConfigItem(string settingName)
        {
            throw new NotImplementedException();
        }

        public void ReloadConfig()
        {
            throw new NotImplementedException();
        }
    }
}
