﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTestTool
{
    public class UploadHelper
    {
        public static long GetUploadBlockSize(long fileSize)
        {
            if (fileSize > 100 * 1024 * 1024)
            {
                // 2MB per block for larger than 100MB
                return 2 * 1024 * 1024;
            }
            if (fileSize > 50 * 1024 * 1024)
            {
                // 1MB per block for larger than 50MB
                return 1024 * 1024;
            }
            return 512 * 1024;
        }

        public static long GetUploadBlockCount(long fileSize, long blockSize)
        {
            if (fileSize > 0)
            {
                long lastBlockSize = fileSize % blockSize;
                if (lastBlockSize == 0)
                {
                    return fileSize / blockSize;
                }
                else
                {
                    return (fileSize - lastBlockSize) / blockSize + 1;
                }
            }
            else
            {
                return 1;
            }
        }
    }
}
