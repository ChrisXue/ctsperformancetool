﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTestTool
{
    public class FileUploadLocalInfo
    {
        public FileInfo FileInfo { get; set; }
        public long UploadBlockSize { get; set; }
        public long UploadBlockCount { get; set; }
        public long FileSize { get; set; }
        public long UploadingBlockIndex { get; set; }
        public FileUploadLocalInfo(string filePath)
        {
            this.FileInfo = new FileInfo(filePath);
            UploadingBlockIndex = 0;
        }

        public byte[] ReadCurrentBlock()
        {
            try
            {
                using (FileStream fileStream = this.FileInfo.OpenRead())
                {
                    long offset = this.UploadingBlockIndex * UploadBlockSize;
                    fileStream.Seek(offset, SeekOrigin.Begin);

                    byte[] buffer = new byte[this.UploadBlockSize];
                    int readOutBytesSize = fileStream.Read(buffer, 0, (int)this.UploadBlockSize);
                    if (readOutBytesSize > 0)
                    {
                        if (readOutBytesSize == this.UploadBlockSize)
                        {
                            return buffer;
                        }
                        else
                        {
                            byte[] blockData = new byte[readOutBytesSize];
                            Array.Copy(buffer, blockData, readOutBytesSize);
                            return blockData;
                        }
                    }
                    else
                    {
                        throw new Exception("Error reading file");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
