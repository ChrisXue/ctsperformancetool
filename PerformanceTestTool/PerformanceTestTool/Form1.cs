﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuage.Common.DataAccess.Central;
using Nuage.Common.DataLayer.AzureBlob;
using Nuage.Common.DataLayer.Cache;
using Nuage.Common.Model.Presentation;
using Nuage.Common.Model.ServiceDataModel.File;
using Nuage.Common.Model.Util;
using Nuage.Common.Model.Util.Common;
using Nuage.Common.Server.JsonServiceFacade;
using Nuage.Common.Server.Util;
using Nuage.Common.Tools.Azure;
using Seismic.CTS.Plugin.Core.BlobProvider;
using LibraryKeyVaultSettings = Nuage.Common.Tools.Azure.KeyVaultSettings;

namespace PerformanceTestTool
{
    public partial class Form1 : Form
    {
        private AuthToken token;
        private CTSBlobProvider blobProvider;
        private CsvExport csvEx;
        public Form1()
        {
            InitializeComponent();
            ConfigUtil.Config = new ConfigTool();
            InitializeKeyVault();
            NupitchCacheManager.InitCacheProviderFromConfiguration();
            AzureClientEncryptionBlobMigration.BlobEncryptionMigrator = new AzureClientEncryptionBlobMigration();
            token = CentralDataProviderFactory.GetAuthToken(ConfigUtil.GetConfigItem("TokenName"), true);
            token.AccountInfo.ContainerName = ConfigUtil.GetConfigItem("ContainerName");
            blobProvider = new CTSBlobProvider(token, string.Empty);
            blobProvider.SetContainer(token.AccountInfo.ContainerName);
            csvEx = new CsvExport();
        }

        private void btn_Browse_Click(object sender, EventArgs e)
        {
            ofd_UploadFileSelector.Multiselect = true;
            if (ofd_UploadFileSelector.ShowDialog() == DialogResult.OK)
            {
                lb_uploadfiles.DataSource = ofd_UploadFileSelector.SafeFileNames.ToList();
            }
        }

        private async void btn_Upload_Click(object sender, EventArgs e)
        {
            csvEx.Reset();
            SetStateString("Uploading", txt_State);
            await UploadFiles();
            SetStateString("Exporting", txt_State);
            SetStateString("Exporting", txt_Condition, false);
            await csvEx.Export();
            txt_State.Text += "Finish";
            SetStateString("Finish", txt_Condition, false);
        }

        private async Task UploadFiles()
        {
            int index = 0;
            CentralDataProviderFactory.GetAuthToken(ConfigUtil.GetConfigItem("TokenName"), true);
            foreach (var filePath in ofd_UploadFileSelector.FileNames)
            {
                FileUploadLocalInfo fl = new FileUploadLocalInfo(filePath);
                fl.FileSize = fl.FileInfo.Length;
                fl.UploadBlockSize = UploadHelper.GetUploadBlockSize(fl.FileSize);
                fl.UploadBlockCount = UploadHelper.GetUploadBlockCount(fl.FileSize, fl.UploadBlockSize);

                var uploadTask = Upload(fl);
                var blobId = await uploadTask.ContinueWith((tempUploadTask) => ReSaveBlob(tempUploadTask.Result, fl.FileInfo.Name, GetFormat(fl), null));
                csvEx.Add(new ExportItem {
                    ContainerName = ConfigUtil.GetConfigItem("ContainerName"),
                    ContentBlobId = blobId.Result,
                    ContentFileName = fl.FileInfo.Name,
                    ContentFormat = GetFormat(fl),
                    ContentSize = fl.FileSize
                });
                index++;
                SetStateString(string.Format("Finish uploading the {0}th file {1} of total {2} files", index, filePath.Split(new char[] {'\\'}.LastOrDefault()),ofd_UploadFileSelector.FileNames.Count()), txt_Condition, false);
            }
        }

        private async Task<string> Upload(FileUploadLocalInfo file)
        {
            SetStateString("Uploading " + file.FileInfo.Name, txt_State);
            var fileUploadInfo = new FileUploadInfo()
            {
                Id = Guid.NewGuid().ToString(),
                Format = GetFormat(file),
                PartNumber = file.UploadingBlockIndex,
                TotalNumberOfParts = file.UploadBlockCount,
                PartSize = (int)file.UploadBlockSize,
                TotalSize = file.FileInfo.Length,
            };

            byte[] currentFileBlockBytes = file.ReadCurrentBlock();
            string blobId = string.Empty;
            while (file.UploadingBlockIndex < file.UploadBlockCount)
            {
                blobId = await DoUpload(blobProvider, fileUploadInfo, currentFileBlockBytes);
                file.UploadingBlockIndex++;
            }
            return blobId;
        }

        private string GetFormat(FileUploadLocalInfo file)
        {
            string fileExt = file.FileInfo.Extension.ToLower().TrimStart('.');
            return fileExt.ToUpper();
        }

        private async Task<string> ReSaveBlob(string tempUploadBlobId, string fileName, string format, Dictionary<string,string> extraMetadata =null)
        {
            SetStateString("ReSaveBlob " + fileName, txt_State);
            BlobInfo uploadBlob = blobProvider.GetBlob(tempUploadBlobId, false);
            if (uploadBlob == null)
            {
                return string.Empty;
            }

            // Save relevant blobs
            var sourceBlobId = Guid.NewGuid().ToString();
            if (BlobIndexProvider.IsIndeXBlobId(tempUploadBlobId))
            {
                sourceBlobId = BlobIndexProvider.GetBlobIndexId(sourceBlobId);
            }
            BlobInfo sourceBlob = new BlobInfo(sourceBlobId, format, uploadBlob.Data);
            blobProvider.SaveBlob(sourceBlob, extraMetadata);
            blobProvider.DeleteBlob(tempUploadBlobId);
            return sourceBlobId;
        }

        private Task<string> DoUpload(TenantBlobProvider BlobProvider, FileUploadInfo fileUploadInfo, byte[] currentFileBlockBytes)
        {
            return Task.Factory.StartNew(()=>
            FileServiceFacade.UploadFile(BlobProvider, fileUploadInfo, currentFileBlockBytes));
        }

        private void InitializeKeyVault()
        {
            LibraryKeyVaultSettings setting = new LibraryKeyVaultSettings(ConfigUtil.GetConfigItem("AKVUri"), ConfigUtil.GetConfigItem("AKVId"), ConfigUtil.GetConfigItem("AKVCertThumbprint"));
            KeyVaultUtility.KeyVaultPrimary = new KeyVaultUtility(setting);
        }

        delegate void SetTextCallback(string text, TextBox txtbox, bool IsAdd);
        private void SetStateString(string log, TextBox txtbox, bool IsAdd = true)
        {
            if (txtbox.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetStateString);
                this.Invoke(d, new object[] { log, txtbox, IsAdd });
            }
            else
            {
                if (IsAdd)
                {
                    txtbox.Text += log + "\r\n";
                }
                else
                {
                    txtbox.Text = log + "\r\n";
                }
            }
        }
    }
}
